package com.venumwolf.jakeybot.core.listeners;

import com.venumwolf.jakeybot.core.Bot;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class OnGuildJoin extends ListenerAdapter {
    protected Bot bot;

    public OnGuildJoin(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void onGuildJoin(GuildJoinEvent event) {

    }
}
