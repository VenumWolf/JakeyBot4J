package com.venumwolf.jakeybot.core;

import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.venumwolf.jakeybot.core.listeners.OnGuildJoin;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;

import javax.security.auth.login.LoginException;

public class Bot {
    protected CommandClientBuilder client;
    protected EventWaiter waiter;
    protected String ownerId;
    protected String token;


    /**
     * Default bot constructor.
     *
     * @param ownerId The id of the bot owner.
     * @param token   The token to log in with.
     */
    public Bot(String token, String ownerId) {
        this.token = token;
        this.ownerId = ownerId;
        waiter = new EventWaiter();
        client = new CommandClientBuilder();

    }

    /**
     * Starts the bot.
     *
     * @throws LoginException Happens when the bot token isn't valid.
     */
    public void start() throws LoginException {
        client.useDefaultGame();
        client.setOwnerId(ownerId);
        client.addCommands();

        new JDABuilder(AccountType.BOT)
                .setToken(token)
                .setStatus(OnlineStatus.ONLINE)
                .setGame(Game.playing("Loading..."))
                .addEventListener(waiter)
                .addEventListener(new OnGuildJoin(this))
                .addEventListener(client.build())
                .build();
    }
}