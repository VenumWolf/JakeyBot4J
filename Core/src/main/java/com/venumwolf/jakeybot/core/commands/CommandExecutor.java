package com.venumwolf.jakeybot.core.commands;

import com.venumwolf.jakeybot.core.Bot;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class CommandExecutor extends ListenerAdapter {
    protected String commandPrefix;
    protected List<Command> commands;
    protected List<Command> disabledCommands;
    protected Bot bot;

    /**
     * Default constructor.
     * @param bot The main bot instance.
     */
    public CommandExecutor(Bot bot, String commandPrefix) {
        this.commandPrefix = commandPrefix;
        this.commands = new ArrayList<>();
        this.disabledCommands = new ArrayList<>();
        this.bot = bot;
    }

    /**
     * Initialize and register commands.
     * @param bot      The main bot instance.
     * @param commands The commands to add.
     */
    public CommandExecutor(Bot bot, String commandPrefix, List<Command> commands) {
        this.commandPrefix = commandPrefix;
        this.commands = commands;
        this.disabledCommands = new ArrayList<>();
        this.bot = bot;
    }

    /**
     * Register a command.
     * @param command The command to register.
     */
    public void addCommand(Command command) {
        // TODO: Allow registration of a single command.
        throw new NotImplementedException();
    }

    /**
     * Register multiple commands.
     * @param commands The commands to register.
     */
    public void addCommands(List<Command> commands) {
        // TODO: Allow registration of multiple commands.
        throw new NotImplementedException();
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

    }
}
