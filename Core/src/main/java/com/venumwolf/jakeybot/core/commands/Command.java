package com.venumwolf.jakeybot.core.commands;

public abstract class Command {
    protected String name;
    protected String[] alieases;
    protected String shortDescription;
    protected String fullDescription;
    protected String useage;
    protected String context;

    public Command() { }

    /**
     * Execute the command.
     * <br>
     * The meat of the command logic should go here.
     * @param args The arguments of the command (the command message minus the command name and prefix.)
     */
    public abstract void execute(String[] args);
}
