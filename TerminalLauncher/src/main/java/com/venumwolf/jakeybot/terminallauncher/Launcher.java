package com.venumwolf.jakeybot.terminallauncher;

import com.venumwolf.jakeybot.core.Bot;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.net.UnknownHostException;

public class Launcher {
    public static void main(String[] args) {
        Bot bot = new Bot(args[0], args[1]);
        try {
            bot.start();
        } catch (LoginException e) {
            System.out.println(e.getMessage());
        } catch (UnknownHostException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
